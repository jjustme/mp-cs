﻿$(function () {
    var notificationsHub = $.connection.notificationsHub;

    var vm = new AppViewModel();
    registerServerEventHandlers(notificationsHub, vm);
    registerClickHandlers(vm);

    $.connection.hub.start().done(function () {
        console.log("signalr connected");
        notificationsHub.server.getSelf().done(function (self) {
            self = ko.utils.arrayFirst(vm.clients(), function(c) {
                return self.Id == c.Id();
            });

            vm.selectedClient(self);
            ko.applyBindings(vm);
        });
    });
});

function AppViewModel() {
    var self = this;
    self.titles = ko.observableArray();
    self.clients = ko.observableArray();
    self.selectedClient = ko.observable();

    self.selectClient = function(client) {
        self.selectedClient(client);
    };

    self.player = new Player();
    self.addTitleViewModel = new AddTitleViewModel();
};

function Player() {
    var self = this;
    self.audio = new Audio();
    self.video = new Video();

    self.playAudio = function(title) {
        console.log("In playAudio with title:" + title.Url);

        self.video.pauseAndClose();
        self.audio.pause();
        self.audio.position = 0;
        self.audio.src = "/api/play/" + encodeURIComponent(title.Id) + "/audio";
        self.audio.autoplay = true;
    };

    self.playVideo = function(title) {
        console.log("In playVideo with title:" + title.Url);

        self.audio.pause();
        self.audio.src = "";
        self.video.play("/api/play/" + encodeURIComponent(title.Id) + "/video");
    }

    // volume is a value from 0 to 100
    self.setVolume = function(volume) {
        self.audio.volume = volume / 100;
        self.video.volume = volume / 100;
    }

    self.stop = function() {
        self.audio.pause();
        self.audio.position = 0;
        self.audio.src = "";
        self.video.pauseAndClose();
    }
};

function Video() {
    var self = this;
    self.modal = $("#videoPlayerModal");
    self.video = $("#videoPlayer").css({ width: "100%" }).get(0);

    self.modal.on("hidden.bs.modal", function() {
        self.video.pause();
    });

    self.play = function(url) {
        self.video.pause();
        self.video.position = 0;
        self.video.src = url;
        self.video.autoplay = true;
        self.video.controls = true;

        self.modal.modal("show");
    };

    self.pauseAndClose = function() {
        self.video.pause();
        self.video.position = 0;
        self.video.src = "";
        self.modal.modal("hide");
    };
};

function AddTitleViewModel() {
    var self = this;
    self.titleTypes = ko.observableArray();
    self.selectedTitleType = ko.observable();
    self.downloadAudio = ko.observable();
    self.downloadVideo = ko.observable();
    self.broadcasterId = ko.observable();
    self.url = ko.observable();
    self.title = ko.observable();

    self.addTitle = function () {
        switch (self.selectedTitleType().host) {
        case "YouTube":
            addYouTubeTitle();
            break;
        case "Live365":
            addLive365Title();
            break;
        case "Shoutcast":
            addShoutcastTitle();
            break;
        case "Upload a file":
            uploadATitle();
            break;
        default:
        }

        function success(data) {
            console.log("add title post request response:");
            console.log(data);
            self.url("");
            self.broadcasterId("");
            self.title("");

            // reset fle upload input element
            var fileInput = $("#fileInput");
            fileInput.wrap("<form>").closest("form").get(0).reset();
            fileInput.unwrap();

            $("#addTitleModal").modal("hide");
        };

        function makeAddTitleRequest(data) {
            $.ajax({
                url: "/api/jobs",
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: success
            });
        }

        function addYouTubeTitle() {
            var payload = {
                Host: self.selectedTitleType().host,
                Url: self.url(),
                DownloadAudio: self.downloadAudio(),
                DownloadVideo: self.downloadVideo()
            };

            makeAddTitleRequest(payload);
        };

        function addLive365Title() {
            var payload = {
                Host: self.selectedTitleType().host,
                Url: "http://www.live365.com/play/" + self.broadcasterId(),
                BroadcasterId: self.broadcasterId()
            };

            makeAddTitleRequest(payload);
        };

        function addShoutcastTitle() {
            var payload = {
                Host: self.selectedTitleType().host,
                Url: "http://www.shoutcast.com/" + self.broadcasterId(),
                StationId: self.broadcasterId()
            };

            makeAddTitleRequest(payload);
        };

        function uploadATitle() {
            var payload = {
                Metadata: self.title()
            };

            $.ajax({
                url: "/api/titles",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(payload)
            }).done(function(title) {
                var fileToUpload = $("#fileInput").get(0).files[0];

                $.ajax({
                    url: "/api/titles/" + title.Id + "/audio",
                    type: "PUT",
                    data: fileToUpload,
                    processData: false,
                    contentType: false,
                    success: success
                });
            });
        };
    };

    self.titleTypes([
        {
            host: "YouTube",
            video: true,
            audio: true,
            url: true
        },
        {
            host: "Live365",
            broadcasterId: true
        },
        {
            host: "Shoutcast",
            broadcasterId: true
        },
        {
            host: "Upload a file",
            uploadForm: true
        }
    ]);
};

function registerClickHandlers(viewModel) {
    $(".titles-list").on("click", ".title-row", function (e) {
        e.preventDefault();
        var title = ko.dataFor(this);
        if (title.HasAudio()) {
            $.ajax("/api/clients/" + viewModel.selectedClient().Id() + "/now-playing", {
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify({
                    TitleId: title.Id(),
                    PlayAudio: "true"
                })
            });
        } else if (title.HasVideo()) {
            $.ajax("/api/clients/" + viewModel.selectedClient().Id() + "/now-playing", {
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify({
                    TitleId: title.Id(),
                    PlayVideo: "true"
                })
            });
        }
    });

    $(".titles-list").on("contextmenu", ".title-row", function(e) {
        e.preventDefault();

        var title = ko.dataFor(this);
        $(".title-context-menu").remove();
        var contextMenu = $("<div/>").addClass("title-context-menu").addClass("list-group");

        if (title.HasAudio()) {
            $("<a/>").addClass("list-group-item").text("Play Audio").attr("href", "#").on("click", function (playAudioEvent) {
                playAudioEvent.preventDefault();
                $.ajax("/api/clients/" + viewModel.selectedClient().Id() + "/now-playing", {
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        TitleId: title.Id(),
                        PlayAudio: "true"
                    })
                });
            })
            .css({
                display: "block"
            })
            .appendTo(contextMenu);
        }

        if (title.HasVideo()) {
            $("<a/>").addClass("list-group-item").text("Play video").attr("href", "#").on("click", function (playVideoEvent) {
                playVideoEvent.preventDefault();
                $.ajax("/api/clients/" + viewModel.selectedClient().Id() + "/now-playing", {
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        TitleId: title.Id(),
                        PlayVideo: "true"
                    })
                });
            })
            .css({
                    display: "block"
                })
            .appendTo(contextMenu);
        }

        $("<a/>").addClass("list-group-item").text("Delete").attr("href", "#").on("click", function (deleteTitleEvent) {
            deleteTitleEvent.preventDefault();
            $.ajax("/api/titles/" + encodeURIComponent(title.Id()), {
                type: "DELETE"
            });
        })
        .css({
            display: "block"
        })
        .appendTo(contextMenu);

        contextMenu
            .appendTo("body")
            .css({
                top: event.pageY + "px",
                left: event.pageX + "px",
                "z-index": 2,
                position: "absolute"
            });
    });

    $(document).on("click", function () {
        $("div.title-context-menu").hide();
    });

    $(".player-controls-pause").on("click", function() {
        $.ajax("/api/clients/" + viewModel.selectedClient().Id() + "/playback/stop", {
            type: "PUT"
        });
    });
};

function registerServerEventHandlers(hub, viewModel) {
    $.extend(hub.client, {

        AddTitle: function(title) {
            viewModel.titles.push(ko.mapping.fromJS(title));
        },

        RemoveTitle: function(title) {
            viewModel.titles.remove(function(t) {
                return t.Id() == title.Id;
            });
        },

        UpdateTitle: function(title) {
            var titleToUpdate = ko.utils.arrayFirst(viewModel.titles(), function(t) {
                return title.Id == t.Id();
            });

            ko.mapping.fromJS(title, titleToUpdate);
        },

        PlayAudio: viewModel.player.playAudio,

        PlayVideo: viewModel.player.playVideo,

        SetVolume: viewModel.player.setVolume,

        StopPlayback: function() {
            viewModel.player.stop();
        },

        ClientConnected: function(newClient) {
            // map title id to title object.
            newClient.NowPlayingTitle = ko.observable(ko.utils.arrayFirst(viewModel.titles(), function (t) {
                return newClient.NowPlayingTitle == t.Id();
            }));

            var client = ko.mapping.fromJS(newClient);

            // volume slider
            client.Volume.subscribe(_.throttle(function (volume) {
                $.ajax("api/clients/" + client.Id() + "/volume", {
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify({
                        volume: volume
                    })
                });
            }, 100));

            viewModel.clients.push(client);
        },

        ClientDisconnected: function(clientId) {
            viewModel.clients.remove(function(c) {
                return c.Id() == clientId;
            });
        },

        ClientUpdated: function(client) {
            // map title id to title object.
            client.NowPlayingTitle = ko.utils.arrayFirst(viewModel.titles(), function (t) {
                return client.NowPlayingTitle == t.Id();
            });

            var clientToUpdate = ko.utils.arrayFirst(viewModel.clients(), function(c) {
                return c.Id() == client.Id;
            });

            clientToUpdate.NowPlayingTitle(client.NowPlayingTitle);
            clientToUpdate.Volume(client.Volume);
        }
    });
};