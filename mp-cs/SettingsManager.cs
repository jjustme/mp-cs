﻿namespace mp_cs
{
    using System;
    using System.Configuration;

    public static class SettingsManager
    {
        public static string WorkDirectory
        {
            get
            {
                return ConfigurationManager.AppSettings["WorkDirectory"];
            }
        }

        public static int Port
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            }
        }
    }
}
