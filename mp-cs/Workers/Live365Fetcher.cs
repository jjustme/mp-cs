namespace mp_cs.Workers
{
    using System;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Net;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    public class Live365Fetcher
    {
        private readonly string broadcasterId;

        private readonly Func<string, Task> titleUpdatedCallback;

        private readonly Uri url;

        public Live365Fetcher(string broadcasterId, Func<string, Task> titleUpdatedCallback)
        {
            this.broadcasterId = broadcasterId;
            this.titleUpdatedCallback = titleUpdatedCallback;

            const string XmlUrlFormatString = "http://www.live365.com/pls/front?handler=playlist&cmd=view&viewType=xml&handle={0}&maxEntries=1";
            this.url = new Uri(string.Format(XmlUrlFormatString, broadcasterId));
        }

        private string metadata;
        public async Task<TimeSpan> Fetch()
        {
            var result = await this.GetMetadata();
            string newMetadata = string.Format("{0} - {1}", result.artist, result.title);

            if (newMetadata != this.metadata)
            {
                this.metadata = newMetadata;
                await this.titleUpdatedCallback(this.metadata);
                Trace.WriteLine(string.Format("broadcaster: {0}; Title: {1}", this.broadcasterId, this.metadata), "Live365Fetcher");
            }

            return TimeSpan.FromSeconds(Convert.ToInt32(result.refresh));
        }

        private async Task<dynamic> GetMetadata()
        {
            dynamic result = new ExpandoObject();
            var xmlAsString = await new WebClient().DownloadStringTaskAsync(this.url);
            var xDoc = XDocument.Parse(xmlAsString);
            // ReSharper disable PossibleNullReferenceException
            result.refresh = xDoc.Root.Element("Refresh").Value;
            result.title = xDoc.Root.Element("PlaylistEntry").Element("Title").Value;
            result.artist = xDoc.Root.Element("PlaylistEntry").Element("Artist").Value;
            // ReSharper restore PossibleNullReferenceException
            return result;
        }
    }
}