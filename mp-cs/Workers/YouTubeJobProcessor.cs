﻿namespace mp_cs.Workers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    using Microsoft.Practices.ObjectBuilder2;

    using Newtonsoft.Json;

    public class YouTubeJobProcessor
    {
        private const string FFmpegPath = "ffmpeg";

        public YouTubeJobProcessorConfiguration Configuration { get; private set; }

        public async Task<YouTubeJobProcessorResult> ProcessJobAsync()
        {
            // Declare result
            var result = new YouTubeJobProcessorResult();

            /* Steps to take:
             * 1. Download the youtube page html
             * 2. Find youtube player configuration
             * 3. If download links require signature decryption:
             *      Start node application to do decryption
             * 4. Download file(s)
             * 5. Convert to mp3 for audio and streamable mp4 for videos
             */
            // 1. Download the html
            var html = await new WebClient().DownloadStringTaskAsync(this.Configuration.Uri);

            // 2. Get youtube player config
            var ytPlayerConfig = GetYouTubePlayerConfig(html);
            // May aswell get the title since we can at this point
            string title = GetTitle(ytPlayerConfig);
            result.Title = title;

            // 3. Get links and check if they need decryption
            YouTubeFileInfo audio = this.GetAudio(ytPlayerConfig);
            YouTubeFileInfo video = this.GetVideo(ytPlayerConfig);

            if ((audio.RequiresDecryption && this.Configuration.DownloadAudio)
                || ((audio.RequiresDecryption || video.RequiresDecryption) && this.Configuration.DownloadVideo))
            {
                this.DecryptFiles(ytPlayerConfig, new List<YouTubeFileInfo> { audio, video });
            }

            // Add parameter to bypass download speed limit
            this.BypassRate(audio);
            this.BypassRate(video);

            // 4. Download files
            if (this.Configuration.DownloadAudio && this.Configuration.DownloadVideo)
            {
                var files = await this.DownloadAndConvertAudioAndVideo(audio, video, title);
                result.AudioPath = files.AudioPath;
                result.VideoPath = files.VideoPath;
            }
            else
            {
                if (this.Configuration.DownloadAudio)
                {
                    result.AudioPath = await this.DownloadAndConvertAudio(audio, title);
                }

                if (this.Configuration.DownloadVideo)
                {
                    result.VideoPath = await this.DownloadAndConvertVideo(audio, video, title);
                }
            }

            return result;
        }

        private string GetTitle(dynamic ytPlayerConfig)
        {
            return ytPlayerConfig.args.title;
        }

        private async Task<string> DownloadAndConvertAudio(YouTubeFileInfo audio, string title)
        {
            var audioDownloadPath = Path.Combine(this.Configuration.TempDirectory, "audio.m4a");

            // download
            await this.DownloadAudio(audio, audioDownloadPath);

            // convert
            return await this.ConvertAudio(audioDownloadPath, this.Configuration.OutputDirectory, HttpUtility.UrlEncode(title));
        }

        private async Task<string> DownloadAndConvertVideo(YouTubeFileInfo audio, YouTubeFileInfo video, string title)
        {
            var audioDownloadPath = Path.Combine(this.Configuration.TempDirectory, "audio.m4a");
            var videoDownloadPath = Path.Combine(this.Configuration.TempDirectory, "video.m4v");

            // download
            await this.DownloadAudioAndVideo(audio, video, audioDownloadPath, videoDownloadPath);

            // mux
            return await this.MuxAudioAndVideoToVideo(audioDownloadPath, videoDownloadPath, this.Configuration.OutputDirectory, HttpUtility.UrlEncode(title));
        }

        private async Task<dynamic> DownloadAndConvertAudioAndVideo(YouTubeFileInfo audio, YouTubeFileInfo video, string title)
        {
            var audioDownloadPath = Path.Combine(this.Configuration.TempDirectory, "audio.m4a");
            var videoDownloadPath = Path.Combine(this.Configuration.TempDirectory, "video.m4v");

            // download
            await this.DownloadAudioAndVideo(audio, video, audioDownloadPath, videoDownloadPath);

            // mux and convert
            var videoTask = this.MuxAudioAndVideoToVideo(audioDownloadPath, videoDownloadPath, this.Configuration.OutputDirectory, HttpUtility.UrlEncode(title));
            var audioTask = this.ConvertAudio(audioDownloadPath, this.Configuration.OutputDirectory, HttpUtility.UrlEncode(title));

            await Task.WhenAll(videoTask, audioTask);
            return new { AudioPath = audioTask.Result, VideoPath = videoTask.Result };
        }

        private async Task DownloadAudioAndVideo(YouTubeFileInfo audio, YouTubeFileInfo video, string audioDownloadPath, string videoDownloadPath)
        {
            Trace.WriteLine(string.Format("Started downloading video for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            var audioDownloadTask = new WebClient().DownloadFileTaskAsync(audio.Url, audioDownloadPath);
            var videoDownloadTask = new WebClient().DownloadFileTaskAsync(video.Url, videoDownloadPath);

            await Task.WhenAll(audioDownloadTask, videoDownloadTask);
            Trace.WriteLine(string.Format("Finished downloading video for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
        }

        private async Task DownloadAudio(YouTubeFileInfo audio, string downloadPath)
        {
            Trace.WriteLine(string.Format("Started downloading audio for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            await new WebClient().DownloadFileTaskAsync(audio.Url, downloadPath);
            Trace.WriteLine(string.Format("Finished downloading audio for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
        }

        private async Task<string> MuxAudioAndVideoToVideo(string audioPath, string videoPath, string outputDirectory, string escapedTitle)
        {
            var outputPath = string.Format("{0}.mp4", Path.Combine(outputDirectory, escapedTitle));
            var ffmpegStartInfo = new ProcessStartInfo(FFmpegPath)
            {
                Arguments = string.Format("-i {0} -i {1} -c:v copy -c:a copy -movflags +faststart {2}", audioPath, videoPath, outputPath),
                UseShellExecute = false,
                RedirectStandardError = true
            };

            var ffmpegProcess = Process.Start(ffmpegStartInfo);
            if (ffmpegProcess == null)
            {
                throw new Exception("Failed to start FFmpeg for job with title: " + escapedTitle);
            }

            File.WriteAllText(Path.Combine(outputDirectory, "audioVideoMux.log"), ffmpegProcess.StandardError.ReadToEnd());
            Trace.WriteLine(string.Format("Starting muxing audio and video for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            await ffmpegProcess.WaitForExitAsync(new CancellationTokenSource(TimeSpan.FromMinutes(1)).Token);
            Trace.WriteLine(string.Format("Finished muxing audio and video for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            return outputPath;
        }

        private async Task<string> ConvertAudio(string audioPath, string outputDirectory, string escapedTitle)
        {
            var outputPath = string.Format("{0}.mp3", Path.Combine(outputDirectory, escapedTitle));
            var ffmpegStartInfo = new ProcessStartInfo(FFmpegPath)
            {
                Arguments = string.Format("-i {0} {1}", audioPath, outputPath),
                UseShellExecute = false,
                RedirectStandardError = true
            };

            var ffmpegProcess = Process.Start(ffmpegStartInfo);
            if (ffmpegProcess == null)
            {
                throw new Exception("Failed to start FFmpeg for job with title: " + escapedTitle);
            }

            File.WriteAllText(Path.Combine(outputDirectory, "audioConvert.log"), ffmpegProcess.StandardError.ReadToEnd());
            Trace.WriteLine(string.Format("Started converting audio for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            await ffmpegProcess.WaitForExitAsync(new CancellationTokenSource(TimeSpan.FromMinutes(1)).Token);
            Trace.WriteLine(string.Format("Finished converting audio for job {0}", this.Configuration.JobId), "YouTubeJobProcessor");
            return outputPath;
        }

        private void BypassRate(YouTubeFileInfo fileInfo)
        {
            fileInfo.Url = new Uri(fileInfo.Url + "&ratebypass=yes");
        }

        private void DecryptFiles(dynamic config, IEnumerable<YouTubeFileInfo> filesToDecrypt)
        {
            var decrypter = new YouTubeDecrypter(config, this.Configuration.TempDirectory);
            filesToDecrypt.ForEach(
                file =>
                    {
                        if (file.RequiresDecryption)
                        {
                            file.Url = new Uri(string.Format("{0}&signature={1}", file.Url, decrypter.Decrypt(file.EncipheredSignature)));
                            file.RequiresDecryption = false;
                        }
                    });
        }

        private YouTubeFileInfo GetVideo(dynamic config)
        {
            IEnumerable<YouTubeFileInfo> videoFiles = GetFiles(config, "video/mp4");
            return videoFiles.OrderByDescending(f => f.Bitrate).First();
        }

        private YouTubeFileInfo GetAudio(dynamic config)
        {
            IEnumerable<YouTubeFileInfo> audioFiles = GetFiles(config, "audio/mp4");
            return audioFiles.OrderByDescending(f => f.Bitrate).First();
        }

        private IEnumerable<YouTubeFileInfo> GetFiles(dynamic config, string type)
        {
            var allFilesInfoRaw = ((string)config.args.adaptive_fmts).Split(',');
            return allFilesInfoRaw.Select(ParseRawFileInfo).Where(f => f.Type.Contains(type));
        }

        private YouTubeFileInfo ParseRawFileInfo(string queryString)
        {
            var requiresDecryption = false;
            var queryObject = HttpUtility.ParseQueryString(queryString);
            if (queryObject["s"] != null)
            {
                requiresDecryption = true;
            }

            return new YouTubeFileInfo
                       {
                           Url = new Uri(queryObject["url"]),
                           RequiresDecryption = requiresDecryption,
                           EncipheredSignature = queryObject["s"],
                           Type = queryObject["type"],
                           Bitrate = Int32.Parse(queryObject["bitrate"])
                       };
        }

        private dynamic GetYouTubePlayerConfig(string html)
        {
            var match = Regex.Match(html, "ytplayer.config *?= *?({.+?});", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                var config = match.Groups[1].Value;
                return JsonConvert.DeserializeObject(config);
            }

            throw new ArgumentException("Html did not contain youtube player config");
        }

        public YouTubeJobProcessor(YouTubeJobProcessorConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            this.Configuration = configuration;
        }

        public class YouTubeFileInfo
        {
            public Uri Url { get; set; }

            public bool RequiresDecryption { get; set; }

            public string EncipheredSignature { get; set; }

            public string Type { get; set; }

            public int Bitrate { get; set; }
        }
    }

    public class YouTubeJobProcessorConfiguration
    {
        public string JobId { get; set; }

        public Uri Uri { get; set; }

        public bool DownloadAudio { get; set; }

        public bool DownloadVideo { get; set; }

        // Any working files can be written here.
        public string TempDirectory { get; set; }

        // Output files like the video and audio should be written in this directory.
        public string OutputDirectory { get; set; }
    }

    // If the job processor succeeds, an instance of this class is returned.
    public class YouTubeJobProcessorResult
    {
        public string Title { get; set; }

        public string AudioPath { get; set; }

        public string VideoPath { get; set; }
    }

    internal class YouTubeDecrypter
    {
        private const string NodePath = "node";

        private readonly string workDirectory;

        public readonly ICollection<string> TemporaryFiles = new HashSet<string>();

        private static readonly IEnumerable<string> SignatureFunctionPatterns = new List<string>
        {
            // signature *?= *?(.*?)\(.*?\)
            "signature *?= *?(.*?)\\(.*?\\)",
            // set\((?:"|')signature(?:"|'), *?(.*?)\(.*?\)\)  example: a.set("signature", cr(c)); where "cr" is the function name we want.
            "set\\((?:\"|')signature(?:\"|'), *?(.*?)\\(.*?\\)\\)",
            // sig\|\|([^\(\{]+?)\(.*?\) example: g=e.sig||ar(e.s) where "ar" is the function name we want.
            "sig\\|\\|([^\\(\\{]+?)\\(.*?\\)"
        };

        private readonly Func<string, string> decrypter; 

        public YouTubeDecrypter(dynamic config, string workDirectory)
        {
            this.workDirectory = workDirectory;

            /* 1. Download the youtube player javascript file
             * 2. Find the decryption function.
             * 3. Gather the decryption function and all the functions
             *    it is dependent on.
             */

            // 1. Download youtube player
            string ytPlayer = new WebClient().DownloadString(string.Format("http:{0}", config.assets.js));

            // 2. Find decryption function
            var signatureFunctionName = string.Empty;
            var found = SignatureFunctionPatterns.Any(
                pattern =>
                {
                    var match = Regex.Match(ytPlayer, pattern, RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        signatureFunctionName = match.Groups[1].Value;
                        return true;
                    }

                    return false;
                });

            if (!found)
            {
                throw new Exception("YouTube decrypt function not found for player: " + config.assets.js);
            }

            // 3. Gather decryption function and dependencies into a string
            var decryptionCode = JavaScriptCodeAccumulator.Process(ytPlayer, signatureFunctionName);

            this.decrypter = GenerateDecrypter(decryptionCode, signatureFunctionName);
        }

        private Func<string, string> GenerateDecrypter(string decryptionCode, string signatureFunctionName)
        {
            return encipheredSignature =>
                {
                    var codeToRun = string.Format("{0}console.log({1}(\"{2}\"));", decryptionCode, signatureFunctionName, encipheredSignature);
                    return this.RunWithNode(codeToRun);
                };
        }

        public string Decrypt(string encipheredSignature)
        {
            return this.decrypter(encipheredSignature);
        }

        private string RunWithNode(string playerCode)
        {
            // write code to file
            var playerCodePath = Path.Combine(this.workDirectory, Path.ChangeExtension(Path.GetRandomFileName(), "log"));
            this.TemporaryFiles.Add(this.workDirectory.GetRelative(playerCodePath));
            File.WriteAllText(playerCodePath, playerCode);

            var nodeStartInfo = new ProcessStartInfo(NodePath)
                                    {
                                        Arguments = playerCodePath,
                                        UseShellExecute = false,
                                        RedirectStandardOutput = true
                                    };

            var nodeProcess = Process.Start(nodeStartInfo);
            if (nodeProcess == null)
            {
                throw new Exception("Failed to start node process.");
            }

            return nodeProcess.StandardOutput.ReadToEnd();
        }
    }

    public class JavaScriptCodeAccumulator
    {
        private static readonly IEnumerable<Func<string, string>> JavascriptFunctionPatterns = new List<Func<string, string>>
                                                                                                   {
                                                                                                       fnName => string.Format("(function {0}\\(.*?\\) *?\\{{(.*?)\\}};)", Regex.Escape(fnName)), // function ar\(.*?\) *?\{(.*?)\}; where "ar" is what we want
                                                                                                       fnName => string.Format("(var {0} *?= *?\\{{(.*?)\\}};)", Regex.Escape(fnName)) // var sq *?= *?\{.*?\};
                                                                                                   };

        private static readonly IEnumerable<string> MethodCallPatterns = new List<string>
                                                                            {
                                                                                "^([^ {{}}().]+?)\\.[^ {{}}().]+?\\([^ {{}}().]+?\\)" // ^([^ {}().]+?)\.[^ {}().]+?\([^ {}().]+?\) example: sq.BT(a, 10) capturing sq
                                                                            };

        private string AccumulatedCode { get; set; }

        private ISet<string> FoundFunctions { get; set; }  

        public static string Process(string javascript, string functionName)
        {
            var javascriptAccumulator = new JavaScriptCodeAccumulator();
            return javascriptAccumulator.Accumulate(javascript, functionName);
        }

        public string Accumulate(string javascript, string functionName)
        {
            var foundFunction = string.Empty;
            var foundFunctionBody = string.Empty;
            var didFindFunction = JavascriptFunctionPatterns.Any(
                pattern =>
                {
                    var match = Regex.Match(javascript, pattern(functionName));
                    if (match.Success)
                    {
                        foundFunction = match.Groups[1].Value;
                        foundFunctionBody = match.Groups[2].Value;
                        this.FoundFunctions.Add(functionName);
                        return true;
                    }

                    return false;
                });

            if (!didFindFunction)
            {
                throw new Exception("Function not found.");
            }

            this.AccumulatedCode += foundFunction;

            this.CheckForMethodCalls(javascript, foundFunctionBody);

            return this.AccumulatedCode;
        }

        /// <summary>
        /// In a function such as:
        /// function er(a){a=a.split("");dr.wD(a,3);dr.sE(a,15);dr.wD(a,2);dr.sE(a,30);dr.sE(a,11);return a.join("")};
        /// This function should find the symbol "dr" and attempt to find it.
        /// </summary>
        /// <param name="javascript"></param>
        /// <param name="functionBody">Function body of the function to check for method calls</param>
        private void CheckForMethodCalls(string javascript, string functionBody)
        {
            IEnumerable<string> statements = functionBody.Split(';');
            statements = statements.Skip(1).Take(statements.Count() - 2);

            statements.ForEach(
                statement =>
                    {
                        var foundFunctionName = string.Empty;
                        var didFindFunctionName = MethodCallPatterns.Any(
                            mcp =>
                                {
                                    var match = Regex.Match(statement, mcp);
                                    if (match.Success)
                                    {
                                        foundFunctionName = match.Groups[1].Value;
                                        return true;
                                    }

                                    return false;
                                });

                        if (didFindFunctionName && !this.FoundFunctions.Contains(foundFunctionName))
                        {
                            this.Accumulate(javascript, foundFunctionName);
                        }
                    });
        }

        public JavaScriptCodeAccumulator()
        {
            this.AccumulatedCode = string.Empty;
            this.FoundFunctions = new HashSet<string>();
        }
    }

    public static class GeneralExtensionMethods
    {
        /// <summary>
        /// Waits asynchronously for the process to exit.
        /// </summary>
        /// <param name="process">The process to wait for cancellation.</param>
        /// <param name="cancellationToken">A cancellation token. If invoked, the task will return 
        /// immediately as canceled.</param>
        /// <returns>A Task representing waiting for the process to end.</returns>
        public static Task WaitForExitAsync(this Process process,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var tcs = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => tcs.TrySetResult(null);
            if (cancellationToken != default(CancellationToken)) cancellationToken.Register(() => tcs.TrySetCanceled());

            return tcs.Task;
        }

        public static string GetRelative(this string fromPath, string toPath)
        {
            return new Uri(fromPath).MakeRelativeUri(new Uri(toPath)).ToString();
        }
    }
}