namespace mp_cs.Workers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;

    using Newtonsoft.Json.Linq;

    public class ShoutcastFetcher
    {
        private const string ShoutcastMetadataUrl = "http://www.shoutcast.com/Player/GetCurrentTrack";

        private static readonly MediaTypeFormatter[] ShoutcastMetadataResponseFormatters = {
                                                                                     new ShoutcastResponseFormatter()
                                                                                 };

        private readonly string stationId;

        private readonly Func<string, Task> titleUpdatedCallback;

        public ShoutcastFetcher(string stationId, Func<string, Task> titleUpdatedCallback)
        {
            this.stationId = stationId;
            this.titleUpdatedCallback = titleUpdatedCallback;
        }

        private string metadata;
        public async Task<TimeSpan> Fetch()
        {
            var response = await this.GetCurrenTrack();

            if (response.Station.CurrentTrack != this.metadata)
            {
                this.metadata = response.Station.CurrentTrack;
                await this.titleUpdatedCallback(this.metadata);
                Trace.WriteLine(string.Format("broadcaster: {0}; Title: {1}", response.Station.Name, this.metadata), "ShoutcastFetcher");
            }

            return response.CallbackDelay;
        }

        private async Task<ShoutcastMetadataResponse> GetCurrenTrack()
        {
            using (var client = new HttpClient())
            {
                var requestParams = new Collection<KeyValuePair<string, string>>
                                        {
                                            new KeyValuePair<string, string>(
                                                "stationID",
                                                this.stationId)
                                        };

                var content = new FormUrlEncodedContent(requestParams);

                var response = await client.PostAsync(ShoutcastMetadataUrl, content);

                return await response.Content.ReadAsAsync<ShoutcastMetadataResponse>(ShoutcastMetadataResponseFormatters);
            }
        }

        public class ShoutcastResponseFormatter : JsonMediaTypeFormatter
        {
            public override bool CanReadType(Type type)
            {
                return type == typeof(ShoutcastMetadataResponse);
            }

            public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
            {
                var jsonObject = JObject.Parse(await content.ReadAsStringAsync());

                return new ShoutcastMetadataResponse
                           {
                               CallbackDelay =
                                   TimeSpan.FromMilliseconds(jsonObject["CallbackDelay"]
                                   .ToObject<long>()),
                               Station =
                                   jsonObject["Station"]
                                   .ToObject<ShoutcastMetadataResponse.ShoutcastStation>()
                           };
            }
        }

        public class ShoutcastMetadataResponse
        {
            public TimeSpan CallbackDelay { get; set; }

            public ShoutcastStation Station { get; set; }

            public class ShoutcastStation
            {
                public int Bitrate { get; set; }

                public string CurrentTrack { get; set; }

                public string Format { get; set; }

                public string Genre { get; set; }

                public string IceUrl { get; set; }

                public string ID { get; set; }

                public bool IsPlaying { get; set; }

                public bool IsRadionomy { get; set; }

                public int Listeners { get; set; }

                public string Name { get; set; }
            }
        }
    }
}