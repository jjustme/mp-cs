﻿namespace mp_cs.Workers
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using System.Xml.XPath;

    public class StreamTheWorldFetcher
    {
        private const string StreamTheWorldMetadataUrl = "http://playerservices.streamtheworld.com/public/nowplaying?mountName={0}&numberToFetch=1";

        private static readonly MediaTypeFormatter[] StreamTheWorldMetadataResponseFormatters = {
                                                                                     new StreamTheWorldResponseFormatter()
                                                                                 };

        private readonly string stationId;

        private readonly Func<string, Task> titleUpdatedCallback;

        public StreamTheWorldFetcher(string stationId, Func<string, Task> titleUpdatedCallback)
        {
            this.stationId = stationId;
            this.titleUpdatedCallback = titleUpdatedCallback;
        }

        private string metadata;
        public async Task<TimeSpan> Fetch()
        {
            var response = await this.GetMetadataInfo();

            var newMetadata = string.Format("{0} - {1}", response.Artist, response.Title);
            if (newMetadata != this.metadata)
            {
                this.metadata = newMetadata;
                await this.titleUpdatedCallback(this.metadata);
                Trace.WriteLine(string.Format("broadcaster: {0}; Title: {1}", response.StationName, this.metadata), "StreamTheWorldFetcher");
            }

            //return response.StartTime + response.Duration - DateTime.UtcNow;
            // Until I can find a better time delay, as the above seems to not be accurate, just use 10 seconds.
            return TimeSpan.FromSeconds(10);
        }

        private async Task<StreamTheWorldMetadataResponse> GetMetadataInfo()
        {
            using (var client = new HttpClient())
            {
                var metadataUrl = string.Format(StreamTheWorldMetadataUrl, this.stationId);

                using (var response = await client.GetAsync(metadataUrl))
                {
                    return await response.Content.ReadAsAsync<StreamTheWorldMetadataResponse>(StreamTheWorldMetadataResponseFormatters);
                }
            }
        }
    }

    internal struct StreamTheWorldMetadataResponse
    {
        public string Title { get; set; }

        public string Artist { get; set; }

        public TimeSpan Duration { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime Timestamp { get; set; }

        public string StationName { get; set; }
    }

    internal class StreamTheWorldResponseFormatter : XmlMediaTypeFormatter
    {
        public override bool CanReadType(Type type)
        {
            return type == typeof(StreamTheWorldMetadataResponse) || base.CanReadType(type);
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (type != typeof(StreamTheWorldMetadataResponse))
            {
                return base.ReadFromStreamAsync(type, readStream, content, formatterLogger);
            }

            var xmlDoc = XDocument.Load(await content.ReadAsStreamAsync());
            var nowPlayingInfo = xmlDoc.Root.Element("nowplaying-info");

            var timestamp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var response = new StreamTheWorldMetadataResponse();
            response.StationName = nowPlayingInfo.Attribute("mountName").Value;
            response.Timestamp = timestamp.AddSeconds(long.Parse(nowPlayingInfo.Attribute("timestamp").Value));
            response.Duration = TimeSpan.FromMilliseconds(long.Parse(nowPlayingInfo.XPathSelectElement("./property[@name='cue_time_duration']").Value));
            response.StartTime = timestamp.AddMilliseconds(long.Parse(nowPlayingInfo.XPathSelectElement("./property[@name='cue_time_start']").Value));
            response.Title = nowPlayingInfo.XPathSelectElement("./property[@name='cue_title']").Value;
            response.Artist = nowPlayingInfo.XPathSelectElement("./property[@name='track_artist_name']").Value;

            return response;
        }
    }
}
