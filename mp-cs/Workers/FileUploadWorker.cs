﻿using System.Threading.Tasks;

namespace mp_cs.Workers
{
    using System.IO;
    using System.Net.Http;

    using mp_cs.Model.Titles;

    public class FileUploadWorker
    {
        private readonly LocalFileTitle title;

        private readonly string temporaryDirectory;

        private readonly string finalDirectory;

        private readonly HttpContent httpContent;

        public FileUploadWorker(LocalFileTitle title, HttpRequestMessage fileUploadRequest, string temporaryDirectory, string finalDirectory)
        {
            this.title = title;
            this.temporaryDirectory = temporaryDirectory;
            this.finalDirectory = finalDirectory;

            this.httpContent = fileUploadRequest.Content;
        }

        public async Task ProcessAsync()
        {
            var tempPath = Path.Combine(this.temporaryDirectory, Path.ChangeExtension(Path.GetRandomFileName(), ".mp3"));
            using (var fileStream = File.Create(tempPath))
            {
                var uploadingFileStream = await this.httpContent.ReadAsStreamAsync();

                await uploadingFileStream.CopyToAsync(fileStream);
            }

            var finalPath = Path.Combine(this.finalDirectory, Path.GetFileName(tempPath));

            File.Move(tempPath, finalPath);

            title.AudioFilePath = finalPath;
            title.HasAudio = true;
        }
    }
}
