﻿namespace mp_cs.Workers
{
    using System;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    public class MetadataFetcher
    {
        private readonly ConcurrentDictionary<Guid, FetcherTask> fetcherTasks = new ConcurrentDictionary<Guid, FetcherTask>(); 

        public async void Start(Guid id, Func<Task<TimeSpan>> fetcher)
        {
            if (fetcher == null) return;

            // keep a reference of the task so we can stop it.
            var fetcherTask = new FetcherTask
            {
                Fetch = fetcher,
                CancellationTokenSource = new CancellationTokenSource()
            };

            if (!this.fetcherTasks.TryAdd(id, fetcherTask))
            {
                return;
            }

            while (fetcherTask.CancellationTokenSource.IsCancellationRequested == false)
            {
                var retryDelay = TimeSpan.Zero;

                try
                {
                    await Task.Run(
                        () =>
                            {
                                retryDelay = fetcherTask.Fetch().Result;
                            });

                    if (retryDelay < TimeSpan.Zero)
                    {
                        throw new InvalidOperationException(string.Format("'{0}' returned a negative retry delay.", id));
                    }
                }
                catch (Exception e)
                {
                    Trace.WriteLine(string.Format("Exception occured fetching for '{0}'. Message: '{1}'.", id, e.Message));
                    retryDelay = TimeSpan.FromSeconds(10);
                }

                try
                {
                    await Task.Delay(retryDelay, fetcherTask.CancellationTokenSource.Token);
                }
                catch (TaskCanceledException tce)
                {
                    Trace.WriteLine(string.Format("Exception occured fetching for '{0}'. Message: '{1}'.", id, tce.Message));
                }
            }
        }

        public void Stop(Guid id)
        {
            FetcherTask fetcherTask;
            if (this.fetcherTasks.TryGetValue(id, out fetcherTask))
            {
                fetcherTask.CancellationTokenSource.Cancel();
                this.fetcherTasks.TryRemove(id, out fetcherTask);
                Trace.WriteLine(string.Format("Stopped fetching for '{0}'", id));
            }
        }
    }

    public class FetcherTask
    {
        public Func<Task<TimeSpan>> Fetch { get; set; }

        public CancellationTokenSource CancellationTokenSource { get; set; }
    }
}