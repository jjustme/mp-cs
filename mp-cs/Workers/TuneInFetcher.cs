namespace mp_cs.Workers
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using System.Xml.XPath;

    public class TuneInFetcher
    {
        private const string RadioTimeMetadataUrlFormat = "http://opml.radiotime.com/Describe.ashx?c=nowplaying&id=s{0}";

        private const string TuneInMetadataUrlFormat = "http://feed.tunein.com/json/livenowjson.aspx?stationId={0}";

        private static readonly MediaTypeFormatter[] RadioTimeMetadataResponseFormatters = { new RadioTimeResponseFormatter() };

        private readonly string stationId;

        private readonly Func<string, Task> titleUpdatedCallback;

        public TuneInFetcher(string stationId, Func<string, Task> titleUpdatedCallback)
        {
            this.stationId = stationId;
            this.titleUpdatedCallback = titleUpdatedCallback;
            this.stationName = new AsyncLazy<string>(() => this.GetStationName());
        }

        private readonly AsyncLazy<string> stationName;
        private string metadata;
        public async Task<TimeSpan> Fetch()
        {
            var response = await this.GetMetadataInfo();

            var newMetadata = string.Format("{0} - {1}", response.Artist, response.Title);
            if (newMetadata != this.metadata)
            {
                this.metadata = newMetadata;
                await this.titleUpdatedCallback(this.metadata);
                Trace.WriteLine(string.Format("broadcaster: {0}; Title: {1}", await this.stationName, this.metadata), "TuneInFetcher");
            }

            return TimeSpan.FromSeconds(10);
        }

        private async Task<TuneInMetadataResponse> GetMetadataInfo()
        {
            using (var client = new HttpClient())
            {
                var metadataUrl = string.Format(TuneInMetadataUrlFormat, this.stationId);

                using (var response = await client.GetAsync(metadataUrl))
                {
                    return await response.Content.ReadAsAsync<TuneInMetadataResponse>();
                }
            }
        }

        private async Task<string> GetStationName()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var metadataUrl = string.Format(RadioTimeMetadataUrlFormat, this.stationId);

                    using (var response = await client.GetAsync(metadataUrl))
                    {
                        var stationInfo = await response.Content.ReadAsAsync<RadioTimeMetadataResponse>(RadioTimeMetadataResponseFormatters);
                        return stationInfo.StationName;
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(string.Format("Failed to station name for station with id '{0}', exception: '{1}'", this.stationId, e), "TuneInFetcher");
                return "Unknown";
            }
        }
    }

    internal class TuneInMetadataResponse
    {
        public string Artist { get; set; }

        public string Title { get; set; }
    }

    internal class RadioTimeMetadataResponse
    {
        public string StationName { get; set; }

        public string Metadata { get; set; }
    }

    internal class RadioTimeResponseFormatter : XmlMediaTypeFormatter
    {
        public override bool CanReadType(Type type)
        {
            return type == typeof(RadioTimeMetadataResponse) || base.CanReadType(type);
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (type != typeof(RadioTimeMetadataResponse))
            {
                return base.ReadFromStreamAsync(type, readStream, content, formatterLogger);
            }

            var xmlDoc = XDocument.Load(await content.ReadAsStreamAsync());

            var stationName = xmlDoc.XPathEvaluate("string(/opml/body/outline[1]/@text)").ToString();
            var metadata = xmlDoc.XPathEvaluate("string(/opml/body/outline[2]/@text)").ToString();

            return new RadioTimeMetadataResponse { StationName = stationName, Metadata = metadata };
        }
    }

    public class AsyncLazy<T> : Lazy<Task<T>>
    {
        public AsyncLazy(Func<T> valueFactory) :
            base(() => Task.Run(valueFactory)) { }

        public AsyncLazy(Func<Task<T>> taskFactory) :
            base(() => Task.Run(taskFactory)) { }

        public TaskAwaiter<T> GetAwaiter() { return Value.GetAwaiter(); }
    }
}