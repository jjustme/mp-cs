﻿namespace mp_cs.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using mp_cs.Managers;
    using mp_cs.Model.Jobs;

    using Newtonsoft.Json;

    public class JobsController : ApiController
    {
        private readonly JobManager jobManager;
       
        [HttpPost]
        public async Task<HttpResponseMessage> CreateJob()
        {
            Job job;

            try
            {
                var jsonString = await this.Request.Content.ReadAsStringAsync();
                job = JsonConvert.DeserializeObject<Job>(jsonString);

                if (!job.Url.IsAbsoluteUri)
                {
                    throw new ArgumentException("Job url should be absolute.");
                }

                // convert job to a lower level class.
                job = JobManager.JobFactory[job.Host](jsonString);
                jobManager.CreateJob(job);
            }
            catch (InvalidOperationException e)
            {
                Trace.WriteLine("Failed to create job", "JobsController");
                return this.Request.CreateErrorResponse(HttpStatusCode.Conflict, e);
            }
            catch (ArgumentException e)
            {
                Trace.WriteLine("Failed to create job", "JobsController");
                return this.Request.CreateErrorResponse(HttpStatusCode.PreconditionFailed, e);
            }
            catch (Exception e)
            {
                Trace.WriteLine("Failed to create job", "JobsController");
                return this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

            Trace.WriteLine("Job created for: " + job.Url, "JobsController");
            return this.Request.CreateResponse(HttpStatusCode.Created, job);
        }

        [HttpGet]
        public IEnumerable<Job> GetAllJobs()
        {
            try
            {
                return this.jobManager.GetAllJobs();
            }
            catch (Exception e)
            {
                Trace.WriteLine("Error getting all jobs: " + e.Message);
                throw;
            }
        }

        public JobsController(JobManager jobManager)
        {
            this.jobManager = jobManager;
        }
    }
}
