﻿namespace mp_cs.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using System.Web.Http;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;

    [RoutePrefix("api/play")]
    public class PlayController : ApiController
    {
        private readonly TitleManager titleManager;

        [HttpGet]
        [Route("{titleId}/audio")]
        public async Task<HttpResponseMessage> PlayAudio(Guid titleId)
        {
            return await this.Play(titleId, true);
        }

        [HttpGet]
        [Route("{titleId}/video")]
        public async Task<HttpResponseMessage> PlayVideo(Guid titleId)
        {
            return await this.Play(titleId, video: true);
        }

        private async Task<HttpResponseMessage> Play(Guid titleId, bool audio = false, bool video = false)
        {
            var title = await this.titleManager.GetTitleAsync(titleId);
            if (title == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if ((!audio || !title.HasAudio) && (!video || !title.HasVideo))
            {
                return this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError());
            }

            StreamInfo streamInfo;
            if (audio && title.HasAudio)
            {
                streamInfo = await title.GetAudio();
            }
            else
            {
                streamInfo = await title.GetVideo();
            }

            // can't seek radio streams
            if (this.Request.Headers.Range != null && !(title is BroadcastTitle))
            {
                return this.StreamPartial(streamInfo, this.Request.Headers.Range);
            }

            return await this.StreamSimple(streamInfo);
        }

        private HttpResponseMessage StreamPartial(StreamInfo streamInfo, RangeHeaderValue rangeHeader)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.PartialContent);
            response.Content = new ByteRangeStreamContent(streamInfo.Content, rangeHeader, streamInfo.ContentType);
            return response;
        }

        private Task<HttpResponseMessage> StreamSimple(StreamInfo streamInfo)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StreamContent(streamInfo.Content);
            if (streamInfo.ContentLength > 0)
            {
                response.Content.Headers.ContentLength = streamInfo.ContentLength;
            }

            response.Content.Headers.ContentType = new MediaTypeHeaderValue(streamInfo.ContentType);
            response.Headers.AcceptRanges.Add("bytes");
            return Task.FromResult(response);
        }

        public PlayController(TitleManager titleManager)
        {
            this.titleManager = titleManager;
        }
    }
}
