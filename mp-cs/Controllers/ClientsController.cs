﻿namespace mp_cs.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Microsoft.AspNet.SignalR;

    using mp_cs.Managers;
    using mp_cs.Model;
    using mp_cs.Repositories;

    [RoutePrefix("api/clients")]
    public class ClientsController : ApiController
    {
        private readonly IHubContext hubContext;

        private readonly ClientsRepository clientsRepository;

        private readonly TitleManager titleManager;

        [HttpPut]
        [Route("{clientId}/now-playing")]
        public async Task SetNowPlaying(string clientId, ClientPlayRequest request)
        {
            var client = this.clientsRepository.TryGetClient(clientId);
            if (client == null)
            {
                throw new HttpResponseException(this.Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Client with id '{0}' not found.", clientId)));
            }

            if (request.PlayAudio && request.PlayVideo)
            {
                throw new HttpResponseException(this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Cannot play both audio and video at the same time!"));
            }

            var title = await this.titleManager.GetTitleAsync(request.TitleId);
            if (title == null)
            {
                throw new HttpResponseException(this.Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Title with id '{0}' not found.", request.TitleId)));
            }

            if (request.PlayAudio && title.HasAudio)
            {
                client.NowPlayingTitle = title;
                this.hubContext.Clients.Client(clientId).PlayAudio(title);
                this.hubContext.Clients.All.ClientUpdated(client);
                return;
            }

            if (request.PlayVideo && title.HasVideo)
            {
                client.NowPlayingTitle = title;
                this.hubContext.Clients.Client(clientId).PlayVideo(title);
                this.hubContext.Clients.All.ClientUpdated(client);
                return;
            }

            throw new HttpResponseException(this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "The title does not have what you want to play."));
        }

        [HttpPut]
        [Route("{clientId}/volume")]
        public Task SetVolume(string clientId, ClientSetVolumeRequest request)
        {
            var client = this.GetClient(clientId);

            if (request.Volume < 0 || request.Volume > 100)
            {
                throw new HttpResponseException(this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Range of volume is 0 - 100."));
            }

            client.Volume = request.Volume;
            this.hubContext.Clients.Client(clientId).SetVolume(request.Volume);
            this.hubContext.Clients.All.ClientUpdated(client);
            return Task.FromResult(0);
        }

        [HttpPut]
        [Route("{clientId}/playback/stop")]
        public Task SetPlayback(string clientId)
        {
            var client = this.GetClient(clientId);

            client.PlaybackStatus = PlaybackStatus.Stopped;
            client.NowPlayingTitle = null;
            this.hubContext.Clients.Client(clientId).StopPlayback();
            this.hubContext.Clients.All.ClientUpdated(client);
            return Task.FromResult(0);
        }

        public ClientsController(IHubContext hubContext, ClientsRepository clientsRepository, TitleManager titleManager)
        {
            this.hubContext = hubContext;
            this.clientsRepository = clientsRepository;
            this.titleManager = titleManager;
        }

        private Client GetClient(string clientId)
        {
            var client = this.clientsRepository.TryGetClient(clientId);
            if (client == null)
            {
                throw new HttpResponseException(
                    this.Request.CreateErrorResponse(
                        HttpStatusCode.NotFound,
                        string.Format("Client with id '{0}' not found.", clientId)));
            }
            return client;
        }
    }

    public class ClientPlayRequest
    {
        public Guid TitleId { get; set; }

        public bool PlayAudio { get; set; }

        public bool PlayVideo { get; set; }
    }

    public class ClientSetVolumeRequest
    {
        public int Volume { get; set; }
    }
}
