﻿namespace mp_cs.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Web.Http;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;
    using mp_cs.Workers;

    using Newtonsoft.Json;

    [RoutePrefix("api/titles")]
    public class TitlesController : ApiController
    {
        private readonly TitleManager titleManager;

        [HttpGet]
        public async Task<IEnumerable<Title>> GetAllTitles()
        {
            return await this.titleManager.GetAllTitlesAsync();
        }

        [HttpDelete]
        public async Task DeleteTitle(Guid id)
        {
            await this.titleManager.RemoveTitleAsync(id);
        }

        [HttpPost]
        public async Task<Title> CreateTitle()
        {
            var jsonString = await this.Request.Content.ReadAsStringAsync();
            var title = JsonConvert.DeserializeObject<LocalFileTitle>(jsonString);
            return await this.titleManager.AddTitleAsync(new LocalFileTitle { Metadata = title.Metadata });
        }

        [HttpPut]
        [Route("{titleId}/audio")]
        public async Task UpdateTitleAudio(Guid titleId)
        {
            var title = (LocalFileTitle)await this.titleManager.GetTitleAsync(titleId);

            var temporaryDirectory = FileManager.CreateRandomSubDirectory(Path.Combine(Program.WorkDirectory, FileManager.TemporaryDirectory));
            var finalDirectory = FileManager.CreateRandomSubDirectory(Path.Combine(Program.WorkDirectory, FileManager.MediaDirectory));
            var worker = new FileUploadWorker(title, this.Request, temporaryDirectory, finalDirectory);

            await worker.ProcessAsync();
            await this.titleManager.UpdateTitleAsync(title);
        }

        public TitlesController(TitleManager titleManager)
        {
            this.titleManager = titleManager;
        }
    }
}
