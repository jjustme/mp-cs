﻿namespace mp_cs.Repositories
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.Practices.ObjectBuilder2;

    using mp_cs.Model.Titles;

    public class TitlesRepository
    {
        private readonly Func<TitlesContext> titlesContext = () => new TitlesContext();
        private readonly ConcurrentDictionary<Guid, Title> titlesCache = new ConcurrentDictionary<Guid, Title>(); 

        public async Task AddAsync(Title title)
        {
            if (this.titlesCache.TryAdd(title.Id, title))
            {
                using (var context = this.titlesContext())
                {
                    context.Titles.Add(title);
                    await context.SaveChangesAsync();
                }
            }
            else
            {
                Trace.WriteLine(string.Format("Tried to add already exisitng title: '{0}.", title.Url), "TitlesRepository");
            }
        }

        public Task<IEnumerable<Title>> GetAllTitlesAsync()
        {
            return Task.FromResult(this.titlesCache.Values as IEnumerable<Title>);
        }

        public Task<Title> GetTitleAsync(Guid key)
        {
            Title title;
            if (!this.titlesCache.TryGetValue(key, out title))
            {
                Trace.WriteLine(string.Format("Trying to get non existing title: '{0}'", key), "TitlesRepository");
            }

            return Task.FromResult(title);
        }

        public IEnumerable<Title> GetTitlesByUrlAsync(string url)
        {
            return this.titlesCache.Values.Where(title => title.Url == url);
        }

        public async Task UpdateAsync(Title title)
        {
            if (this.titlesCache.ContainsKey(title.Id))
            {
                using (var context = this.titlesContext())
                {
                    context.Titles.Attach(title);
                    context.Entry(title).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
            }
            else
            {
                Trace.WriteLine(string.Format("Attempted to update non existing title: {0}", title.Id), "TitlesRepository");
            }

            Trace.WriteLine("Update title called.", "TitlesRepository");
        }

        public async Task<Title> RemoveAsync(Guid titleId)
        {
            Title removedTitle;
            if (this.titlesCache.TryRemove(titleId, out removedTitle))
            {
                using (var context = this.titlesContext())
                {
                    context.Titles.Attach(removedTitle);
                    context.Titles.Remove(removedTitle);
                    await context.SaveChangesAsync();
                }
            }
            else
            {
                Trace.WriteLine(string.Format("Tried to remove non existing title: '{0}'", removedTitle.Url), "TitlesRepository");
            }

            return removedTitle;
        }

        public TitlesRepository()
        {
            using (var context = this.titlesContext())
            {
                context.Titles.AsParallel().ForEach(title => this.titlesCache.TryAdd(title.Id, title));
            }
        }
    }
}