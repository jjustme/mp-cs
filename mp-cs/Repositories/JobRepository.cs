﻿namespace mp_cs.Repositories
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    using mp_cs.Model.Jobs;

    public class JobRepository
    {
        private readonly ConcurrentDictionary<Uri, Job> jobs = new ConcurrentDictionary<Uri, Job>();

        public bool TryAddJob(Job job)
        {
            return jobs.TryAdd(job.Url, job);
        }

        public bool TryRemove(Job job)
        {
            Job removedJob;
            return jobs.TryRemove(job.Url, out removedJob);
        }

        public IEnumerator<KeyValuePair<Uri, Job>> GetEnumerator()
        {
            return jobs.GetEnumerator();
        }
    }
}
