namespace mp_cs.Repositories
{
    using System.Data.Entity;
    using System.Diagnostics;

    using mp_cs.Model.Titles;

    public class TitlesContext : DbContext
    {
        public TitlesContext() : base("TitlesContext")
        {
            Trace.WriteLine("TitlesContext constructor");
        }

        protected override void Dispose(bool disposing)
        {
            Trace.WriteLine("TitlesContext disposing.");
            base.Dispose(disposing);
        }

        public virtual DbSet<Title> Titles { get; set; }
    }
}