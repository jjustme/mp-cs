﻿namespace mp_cs.Repositories
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;

    using mp_cs.Model;

    public class ClientsRepository
    {
        private readonly ConcurrentDictionary<string, Client> clients = new ConcurrentDictionary<string, Client>();

        public void TryAddClient(Client client)
        {
            if (!this.clients.TryAdd(client.Id, client))
            {
                Trace.WriteLine("Error adding client.", "ClientsRepository");
            }
        }

        public IEnumerable<Client> GetAllClients()
        {
            return this.clients.Values;
        }

        public Client TryGetClient(string id)
        {
            Client client;
            if (!this.clients.TryGetValue(id, out client))
            {
                Trace.WriteLine("Error getting client.", "ClientsRepository");
            }

            return client;
        }

        public void TryRemoveClient(string id)
        {
            Client client;
            if (!this.clients.TryRemove(id, out client))
            {
                Trace.WriteLine("Error removing client.", "ClientsRepository");
            }
        }
    }
}
