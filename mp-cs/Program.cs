﻿namespace mp_cs
{
    using System;

    using Microsoft.Owin.Hosting;

    public class Program
    {
        public static readonly string WorkDirectory = SettingsManager.WorkDirectory;

        public static void Main(string[] args)
        {
            StartServer();
        }

        private static void StartServer()
        {
            string url = string.Format("http://*:{0}", SettingsManager.Port);
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Server listening on {0}.", url);
                Console.ReadLine();
            }
        }
    }
}
