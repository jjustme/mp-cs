﻿namespace mp_cs.Model.Jobs
{
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;

    public class Live365Job : Job
    {
        [Required]
        public string BroadcasterId { get; set; }

        public override async Task<bool> PublishAsync(TitleManager titleManager)
        {
            var title = new Live365Title
                            {
                                Url = this.Url.ToString(),
                                BroadcasterId = this.BroadcasterId
                            };

            await titleManager.AddTitleAsync(title);
            return true;
        }

        public override bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(this.BroadcasterId);
        }

        public override HostType Host
        {
            get
            {
                return HostType.Live365;
            }
        }
    }
}