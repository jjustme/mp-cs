﻿namespace mp_cs.Model.Jobs
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using mp_cs.Managers;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Job
    {
        [Required]
        public Uri Url { get; set; }

        [Required, JsonConverter(typeof(StringEnumConverter))]
        public virtual HostType Host { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public JobState State { get; set; }

        /// <summary>
        /// General status of the job, supposed to be
        /// displayed to user.
        /// </summary>
        public virtual string Status { get; set; }

        [JsonIgnore]
        public string WorkDirectory { get; set; }

        [JsonIgnore]
        public string OutputDirectory { get; set; }

        [JsonIgnore]
        public int RetriesAllowed { get; set; }

        [JsonIgnore]
        public int RetryCount { get; set; }

        public virtual bool IsValid()
        {
            return true;
        }

        /// <summary>
        /// Processes the job
        /// </summary>
        /// <returns>Bool indicating whether the job was processed successfully.</returns>
        public virtual Task<bool> ProcessAsync()
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> PublishAsync(TitleManager titleManager)
        {
            return Task.FromResult(false);
        }
    }
}
