﻿namespace mp_cs.Model.Jobs
{
    using System.Threading.Tasks;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;

    /// <summary>
    /// LocalFileJob is a job that requires local storage
    /// to download the audio/video before it can be
    /// played.
    /// </summary>
    public abstract class LocalFileJob : Job
    {
        public string Title { get; set; }

        public bool DownloadAudio { get; set; }

        public bool DownloadVideo { get; set; }

        public string AudioFilePath { get; set; }

        public string VideoFilePath { get; set; }

        public override bool IsValid()
        {
            return this.DownloadAudio || this.DownloadVideo;
        }

        public override async Task<bool> PublishAsync(TitleManager titleManager)
        {
            var title = new LocalFileTitle
            {
                Url = this.Url.ToString(),
                Metadata = this.Title,
                HasAudio = this.DownloadAudio,
                HasVideo = this.DownloadVideo,
                AudioFilePath = this.AudioFilePath,
                VideoFilePath = this.VideoFilePath
            };

            await titleManager.AddTitleAsync(title);
            return true;
        }
    }
}