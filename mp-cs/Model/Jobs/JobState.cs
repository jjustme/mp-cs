﻿namespace mp_cs.Model.Jobs
{
    public enum JobState
    {
        Started,
        Succeeded,
        Failed
    }
}