namespace mp_cs.Model.Jobs
{
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;

    public class TuneInJob : Job
    {
        [Required]
        public string StationId { get; set; }

        public override async Task<bool> PublishAsync(TitleManager titleManager)
        {
            var title = new TuneInTitle
            {
                Url = this.Url.ToString(),
                BroadcasterId = this.StationId
            };

            await titleManager.AddTitleAsync(title);
            return true;
        }

        public override bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(this.StationId) && !this.StationId.StartsWith("s");
        }

        public override HostType Host
        {
            get
            {
                return HostType.TuneIn;
            }
        }
    }
}