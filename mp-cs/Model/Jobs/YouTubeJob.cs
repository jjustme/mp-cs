﻿namespace mp_cs.Model.Jobs
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;

    using mp_cs.Workers;

    public class YouTubeJob : LocalFileJob
    {
        public override async Task<bool> ProcessAsync()
        {
            this.State = JobState.Started;

            var startInfo = new YouTubeJobProcessorConfiguration
            {
                JobId = this.Url.ToString(),
                Uri = this.Url,
                DownloadAudio = this.DownloadAudio,
                DownloadVideo = this.DownloadVideo,
                OutputDirectory = this.OutputDirectory,
                TempDirectory = this.WorkDirectory
            };

            var processor = new YouTubeJobProcessor(startInfo);

            YouTubeJobProcessorResult result;
            try
            {
                result = await processor.ProcessJobAsync();
            }
            catch (Exception e)
            {
                this.State = JobState.Failed;
                Trace.WriteLine("Error processing youtube job: " + e.Message);
                throw;
            }

            this.State = JobState.Succeeded;

            this.Title = result.Title;
            if (!string.IsNullOrEmpty(result.AudioPath)) this.AudioFilePath = result.AudioPath;
            if (!string.IsNullOrEmpty(result.VideoPath)) this.VideoFilePath = result.VideoPath;
            return true;
        }
    }
}