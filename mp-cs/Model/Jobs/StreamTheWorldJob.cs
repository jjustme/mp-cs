﻿namespace mp_cs.Model.Jobs
{
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using mp_cs.Managers;
    using mp_cs.Model.Titles;

    public class StreamTheWorldJob : Job
    {
        [Required]
        public string StationId { get; set; }

        public override async Task<bool> PublishAsync(TitleManager titleManager)
        {
            var title = new StreamTheWorldTitle
            {
                Url = this.Url.ToString(),
                BroadcasterId = this.StationId
            };

            await titleManager.AddTitleAsync(title);
            return true;
        }

        public override bool IsValid()
        {
            return !string.IsNullOrWhiteSpace(this.StationId);
        }

        public override HostType Host
        {
            get
            {
                return HostType.StreamTheWorld;
            }
        }
    }
}
