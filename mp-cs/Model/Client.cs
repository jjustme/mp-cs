﻿namespace mp_cs.Model
{
    using mp_cs.Model.Titles;

    using Newtonsoft.Json;

    public class Client
    {
        public string Id { get; set; }

        public string FriendlyName { get; set; }

        public int Volume { get; set; }

        [JsonConverter(typeof(IdOnlySerializer))]
        public Title NowPlayingTitle { get; set; }

        public PlaybackStatus PlaybackStatus { get; set; }
    }

    public enum PlaybackStatus
    {
        Playing,
        Paused,
        Stopped
    }
}