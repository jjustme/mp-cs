﻿namespace mp_cs.Model
{
    using System;

    using mp_cs.Model.Titles;

    using Newtonsoft.Json;

    public class IdOnlySerializer : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var title = value as Title;
            if (title != null)
            {
                writer.WriteValue(title.Id);
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(Title).IsAssignableFrom(objectType);
        }
    }
}