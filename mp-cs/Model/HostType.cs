﻿namespace mp_cs.Model
{
    public enum HostType
    {
        Live365,

        YouTube,

        Shoutcast,

        StreamTheWorld,

        TuneIn
    }
}