﻿namespace mp_cs.Model.Titles
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;

    using mp_cs.Utilities;
    using mp_cs.Workers;

    public class StreamTheWorldTitle : BroadcastTitle
    {
        private const string StreamTheWorldUrlFormat = "http://provisioning.streamtheworld.com/pls/{0}.pls";

        private static readonly MediaTypeFormatter[] StreamTheWorldPlsResponseFormatters = {
                                                                                     new PlsResponseFormatter()
                                                                                 };

        public override Func<Task<TimeSpan>> Fetcher(Func<Title, Task> progressCallback)
        {
            var fetcher = new StreamTheWorldFetcher(this.BroadcasterId, async metadata =>
            {
                this.Metadata = metadata;
                await progressCallback(this);
            });

            return fetcher.Fetch;
        }

        public override async Task<StreamInfo> GetAudio()
        {
            var streamUrl = await this.GetStreamUrl();

            var request = WebRequest.CreateHttp(streamUrl);
            var response = await request.GetResponseAsync();
            return new StreamInfo
            {
                ContentLength = response.ContentLength,
                ContentType = response.ContentType,
                Content = response.GetResponseStream()
            };
        }

        private async Task<Uri> GetStreamUrl()
        {
            using (var client = new HttpClient())
            {
                var plsUrl = string.Format(StreamTheWorldUrlFormat, this.BroadcasterId);

                using (var response = await client.GetAsync(plsUrl))
                {
                    var plsFile = await response.Content.ReadAsAsync<PlsFile>(StreamTheWorldPlsResponseFormatters);

                    return plsFile.Files.OrderBy(rn => Guid.NewGuid()).First();
                }
            }
        }
    }
}
