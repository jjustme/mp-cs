﻿namespace mp_cs.Model.Titles
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Threading.Tasks;

    public abstract class Title : IDisposable
    {
        [Key]
        public Guid Id { get; set; }

        public virtual string Url { get; set; }

        public virtual string Metadata { get; set; }

        public virtual bool HasAudio { get; set; }

        public virtual bool HasVideo { get; set; }

        public virtual void Dispose()
        {
            Trace.WriteLine(string.Format("Disposing title {0}", this.Url), "Title");
        }

        public virtual Task<StreamInfo> GetAudio()
        {
            throw new NotImplementedException();
        }

        public virtual Task<StreamInfo> GetVideo()
        {
            throw new NotImplementedException();
        }

        protected Title()
        {
            this.Id = Guid.NewGuid();
        }
    }
}
