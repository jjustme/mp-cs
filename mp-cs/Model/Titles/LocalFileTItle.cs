﻿namespace mp_cs.Model.Titles
{
    using System.IO;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    public class LocalFileTitle : Title
    {
        [JsonIgnore]
        public string AudioFilePath { get; set; }

        [JsonIgnore]
        public string VideoFilePath { get; set; }

        public override Task<StreamInfo> GetAudio()
        {
            var fileInfo = new FileInfo(this.AudioFilePath);
            return Task.FromResult(new StreamInfo
            {
                Content = fileInfo.OpenRead(),
                ContentLength = fileInfo.Length,
                ContentType = "audio/mpeg"
            });
        }

        public override Task<StreamInfo> GetVideo()
        {
            var fileInfo = new FileInfo(this.VideoFilePath);
            return Task.FromResult(new StreamInfo
            {
                Content = fileInfo.OpenRead(),
                ContentLength = fileInfo.Length,
                ContentType = "video/mp4"
            });
        }
    }
}