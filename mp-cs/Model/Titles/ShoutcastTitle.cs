namespace mp_cs.Model.Titles
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using mp_cs.Workers;

    public class ShoutcastTitle : BroadcastTitle
    {
        private const string ShoutcastStreamUrlUrl = "http://www.shoutcast.com/Player/GetStreamUrl";

        public override Func<Task<TimeSpan>> Fetcher(Func<Title, Task> progressCallback)
        {
            var fetcher = new ShoutcastFetcher(this.BroadcasterId, async metadata =>
                    {
                        this.Metadata = metadata;
                        await progressCallback(this);
                    });

            return fetcher.Fetch;
        }

        public override async Task<StreamInfo> GetAudio()
        {
            var streamUrl = await this.GetStreamUrl();

            var request = WebRequest.CreateHttp(streamUrl);
            var response = await request.GetResponseAsync();
            return new StreamInfo
            {
                ContentLength = response.ContentLength,
                ContentType = response.ContentType,
                Content = response.GetResponseStream()
            };
        }

        private async Task<Uri> GetStreamUrl()
        {
            using (var client = new HttpClient())
            {
                var requestParams = new Collection<KeyValuePair<string, string>>
                                        {
                                            new KeyValuePair<string, string>(
                                                "station",
                                                this.BroadcasterId)
                                        };

                var content = new FormUrlEncodedContent(requestParams);

                var response = await client.PostAsync(ShoutcastStreamUrlUrl, content);

                return await response.Content.ReadAsAsync<Uri>();
            }
        }
    }
}