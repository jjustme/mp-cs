namespace mp_cs.Model.Titles
{
    using System.IO;

    public class StreamInfo
    {
        public long ContentLength { get; set; }

        public string ContentType { get; set; }

        public Stream Content { get; set; }
    }
}