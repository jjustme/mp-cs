namespace mp_cs.Model.Titles
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading.Tasks;

    using mp_cs.Workers;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class TuneInTitle : BroadcastTitle
    {
        private const string TuneInStreamUrlFormat = "http://tunein.com/tuner/tune/?stationId={0}&tuneType=Station";

        private static readonly MediaTypeFormatter[] TuneInStreamResponseFormatters = { new TuneInStreamsFormatter() };

        public override Func<Task<TimeSpan>> Fetcher(Func<Title, Task> progressCallback)
        {
            var fetcher = new TuneInFetcher(
                this.BroadcasterId,
                async metadata =>
                    {
                        this.Metadata = metadata;
                        await progressCallback(this);
                    });

            return fetcher.Fetch;
        }

        public override async Task<StreamInfo> GetAudio()
        {
            var streamUrl = await this.GetStreamUrl();

            var request = WebRequest.CreateHttp(streamUrl);
            var response = await request.GetResponseAsync();
            return new StreamInfo
                       {
                           ContentLength = response.ContentLength,
                           ContentType = response.ContentType,
                           Content = response.GetResponseStream()
                       };
        }

        private async Task<Uri> GetStreamUrl()
        {
            using (var client = new HttpClient())
            {
                var tuneUrl = string.Format(TuneInStreamUrlFormat, this.BroadcasterId);

                using (var tuneResponse = await client.GetAsync(tuneUrl))
                {
                    var tuneObject = await tuneResponse.Content.ReadAsAsync<JObject>();
                    var streamsUrl = tuneObject["StreamUrl"].ToString();

                    using (var streamUrlsResponse = await client.GetAsync(streamsUrl))
                    {
                        var streamUrls = await streamUrlsResponse.Content.ReadAsAsync<TuneInStreamUrlsResponse>(TuneInStreamResponseFormatters);

                        return
                            streamUrls.Streams.OrderByDescending(s => s.Bandwidth)
                                .ThenByDescending(s => s.Reliability)
                                .First()
                                .Url;
                    }
                }
            }
        }
    }

    internal class TuneInStreamUrlsResponse
    {
        public IEnumerable<TuneInStream> Streams { get; set; }
    }

    internal class TuneInStream
    {
        public int Bandwidth { get; set; }

        public int Reliability { get; set; }

        public Uri Url { get; set; }
    }

    internal class TuneInStreamsFormatter : JsonMediaTypeFormatter
    {
        public override bool CanReadType(Type type)
        {
            return type == typeof(TuneInStreamUrlsResponse) || base.CanReadType(type);
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            if (type != typeof(TuneInStreamUrlsResponse))
            {
                return base.ReadFromStreamAsync(type, readStream, content, formatterLogger);
            }

            var responseAsString = await content.ReadAsStringAsync();

            // the json is wrapped in brackets like this: ( %json% );
            // so remove the first character and last two, then try to parse as json

            var jsonString = responseAsString.Substring(1, responseAsString.Length - 3);
            return JsonConvert.DeserializeObject<TuneInStreamUrlsResponse>(jsonString);
        }
    }
}