namespace mp_cs.Model.Titles
{
    using System;
    using System.Net;
    using System.Threading.Tasks;

    using mp_cs.Workers;

    public class Live365Title : BroadcastTitle
    {
        public override Func<Task<TimeSpan>> Fetcher(Func<Title, Task> progressCallback)
        {
            var fetcher = new Live365Fetcher(this.BroadcasterId, async metadata =>
                    {
                        this.Metadata = metadata;
                        await progressCallback(this);
                    });

            return fetcher.Fetch;
        }

        public override async Task<StreamInfo> GetAudio()
        {
            var request = WebRequest.CreateHttp(this.Url + "&ff=30");
            var response = await request.GetResponseAsync();
            return new StreamInfo
            {
                ContentLength = response.ContentLength,
                ContentType = response.ContentType,
                Content = response.GetResponseStream()
            };
        }
    }
}