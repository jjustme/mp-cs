namespace mp_cs.Model.Titles
{
    using System;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    public class BroadcastTitle : Title
    {
        [JsonIgnore]
        public string BroadcasterId { get; set; }

        public virtual Func<Task<TimeSpan>> Fetcher(Func<Title, Task> progressCallback)
        {
            return null;
        }

        public override bool HasAudio
        {
            get
            {
                return true;
            }
        }

        public override bool HasVideo
        {
            get
            {
                return false;
            }
        }
    }
}