﻿namespace mp_cs.Hub
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;
    using Microsoft.Practices.ObjectBuilder2;

    using mp_cs.Model;
    using mp_cs.Repositories;

    public partial class NotificationsHub : Hub
    {
        private readonly TitlesRepository titlesRepository;

        private readonly ClientsRepository clientsRepository;

        public override async Task OnConnected()
        {
            Trace.WriteLine("Client connected.", "NotificationsHub");

            var client = DefaultClient(this.Context.ConnectionId);

            this.clientsRepository.TryAddClient(client);
            this.Clients.Others.ClientConnected(client);

            var titles = await this.titlesRepository.GetAllTitlesAsync();
            titles.ForEach(title => this.Clients.Caller.AddTitle(title));

            var clients = this.clientsRepository.GetAllClients();
            clients.ForEach(c => this.Clients.Caller.ClientConnected(c));
        }

        public override Task OnReconnected()
        {
            Trace.WriteLine("Client reconnected.", "ClientsRepository");

            // if the server restarts, we get a reconnected event
            this.clientsRepository.TryAddClient(DefaultClient(this.Context.ConnectionId));
            return Task.FromResult(0);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Trace.WriteLine("Client disconnected.", "NotificationsHub");
            this.Clients.Others.ClientDisconnected(this.Context.ConnectionId);
            this.clientsRepository.TryRemoveClient(this.Context.ConnectionId);
            return Task.FromResult(0);
        }

        public Task ChangeName(string name)
        {
            var client = this.clientsRepository.TryGetClient(this.Context.ConnectionId);

            if (string.IsNullOrEmpty(name))
            {
                Trace.WriteLine(
                    string.Format("{0} tried to set their name to null or an empty string.", client.FriendlyName),
                    "NotificationsHub");
            }
            else
            {
                Trace.WriteLine(
                    string.Format("{0} will now be known as {1}.", client.FriendlyName, name),
                    "NotificationsHub");
                client.FriendlyName = name;
            }

            return Task.FromResult(0);
        }

        public Client GetSelf()
        {
            return this.clientsRepository.TryGetClient(this.Context.ConnectionId);
        }

        public NotificationsHub(TitlesRepository titlesRepository, ClientsRepository clientsRepository)
        {
            this.titlesRepository = titlesRepository;
            this.clientsRepository = clientsRepository;
        }
    }

    public partial class NotificationsHub
    {
        private static Client DefaultClient(string id)
        {
            return new Client
            {
                Id = id,
                Volume = 50,
                FriendlyName = GetRandomName(),
                PlaybackStatus = PlaybackStatus.Stopped
            };
        }

        private static readonly IList<string> RandomNamesList = new[]
                                                           {
                                                               "TheConscious", "TheTypical", "TheThankful",
                                                               "TheScreeching", "TheHulking", "TheBig", "TheThird",
                                                               "TheHushed", "TheDeadpan", "TheGeneral"
                                                           };
        private static string GetRandomName()
        {
            return RandomNamesList.OrderBy(rn => Guid.NewGuid()).First();
        }
    }
}
