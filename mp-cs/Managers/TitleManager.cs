﻿namespace mp_cs.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    using mp_cs.Model.Titles;
    using mp_cs.Repositories;
    using mp_cs.Workers;

    public class TitleManager
    {
        private readonly TitlesRepository titlesRepository;

        private readonly MetadataFetcher fetcher;

        private readonly IHubContext hubContext;

        public async Task<Title> AddTitleAsync(Title title)
        {
            await this.titlesRepository.AddAsync(title);

            // notify clients that the title has been added.
            this.hubContext.Clients.All.AddTitle(title);

            this.ProcessTitle(title);

            return title;
        }

        public void ProcessTitle(Title title)
        {
            var broadcastTitle = title as BroadcastTitle;
            if (broadcastTitle == null) return;

            // give broadcastTitle opportunity to start fetching metadata if needed.
            var fetcherFunc = broadcastTitle.Fetcher(this.UpdateTitleWithoutSavingAsync);

            this.fetcher.Start(broadcastTitle.Id, fetcherFunc);
        }

        /// <summary>
        /// BroadcastTitles titles's are always changing,
        /// no need to hit the database for every update.
        /// </summary>
        private Task UpdateTitleWithoutSavingAsync(Title title)
        {
            this.hubContext.Clients.All.UpdateTitle(title);
            return Task.FromResult(0);
        }

        public async Task UpdateTitleAsync(Title title)
        {
            this.hubContext.Clients.All.UpdateTitle(title);
            await this.titlesRepository.UpdateAsync(title);
        }

        public async Task<Title> GetTitleAsync(Guid key)
        {
            return await this.titlesRepository.GetTitleAsync(key);
        }

        public async Task<IEnumerable<Title>> GetAllTitlesAsync()
        {
            return await this.titlesRepository.GetAllTitlesAsync();
        }

        public IEnumerable<Title> GetTitlesByUrl(string url)
        {
            return this.titlesRepository.GetTitlesByUrlAsync(url);
        }

        public async Task RemoveTitleAsync(Guid titleId)
        {
            var title = await this.titlesRepository.RemoveAsync(titleId);
            this.fetcher.Stop(titleId);

            // notify clients that the title has been removed.
            this.hubContext.Clients.All.RemoveTitle(title);
        }

        public TitleManager(TitlesRepository titlesRepository, MetadataFetcher fetcher, IHubContext hubContext)
        {
            this.titlesRepository = titlesRepository;
            this.fetcher = fetcher;
            this.hubContext = hubContext;
        }

        ~TitleManager()
        {
            Trace.WriteLine("TitleManager finialiser.");
        }
    }
}
