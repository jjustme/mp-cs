﻿namespace mp_cs.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using mp_cs.Model;
    using mp_cs.Model.Jobs;
    using mp_cs.Repositories;

    using Newtonsoft.Json;

    public class JobManager
    {
        private readonly JobRepository jobRepository;

        private readonly TitleManager titleManager;

        public static readonly IReadOnlyDictionary<HostType, Func<string, Job>> JobFactory = new Dictionary<HostType, Func<string, Job>>
                                                                           {
                                                                               {HostType.Live365, JsonConvert.DeserializeObject<Live365Job>},
                                                                               {HostType.YouTube, JsonConvert.DeserializeObject<YouTubeJob>},
                                                                               {HostType.Shoutcast, JsonConvert.DeserializeObject<ShoutcastJob>},
                                                                               {HostType.StreamTheWorld, JsonConvert.DeserializeObject<StreamTheWorldJob>},
                                                                               {HostType.TuneIn, JsonConvert.DeserializeObject<TuneInJob>}
                                                                           };

        public void CreateJob(Job job)
        {
            if (!job.IsValid()) throw new ArgumentException("Invalid job");

            if (jobRepository.TryAddJob(job))
            {
                // start job
                this.StartJob(job);
            }
            else
            {
                throw new InvalidOperationException("Job for url already exists.");
            }
        }

        public IEnumerable<Job> GetAllJobs()
        {
            var enumerator = this.jobRepository.GetEnumerator();

            while (enumerator.MoveNext())
            {
                yield return enumerator.Current.Value;
            }
        } 

        private async void StartJob(Job job)
        {
            // ensure title doesn't already exist
            if (titleManager.GetTitlesByUrl(job.Url.ToString()).Any())
            {
                Trace.WriteLine(string.Format("Title already exists for {0}. Not processing job.", job.Url), "JobManager");
                return;
            }

            try
            {
                job.WorkDirectory = FileManager.CreateRandomSubDirectory(Path.Combine(Program.WorkDirectory, FileManager.TemporaryDirectory));
                job.OutputDirectory = FileManager.CreateRandomSubDirectory(Path.Combine(Program.WorkDirectory, FileManager.MediaDirectory));
            }
            catch (Exception e)
            {
                Trace.WriteLine(string.Format("Job {0} failed.", job.Url), "JobManager");
                Trace.WriteLine(e.Message, "JobManager");
                this.jobRepository.TryRemove(job);
                return;
            }

            try
            {
                // process the job
                Trace.WriteLine(string.Format("Job {0} starting.", job.Url), "JobManager");
                await job.ProcessAsync();
                Trace.WriteLine(string.Format("Job {0} succeeded.", job.Url), "JobManager");
            }
            catch (Exception e)
            {
                Trace.WriteLine(string.Format("Job {0} failed.", job.Url), "JobManager");
                Trace.WriteLine(e.Message, "JobManager");

                // retry
                if (job.RetriesAllowed > job.RetryCount)
                {
                    Trace.WriteLine("Retrying job: " + job.Url, "JobManager");
                    job.RetryCount++;
                    this.StartJob(job);
                }
                else
                {
                    Trace.WriteLine(string.Format("Job {0} failed {1} times, not retrying.", job.Url, job.RetryCount), "JobManager");
                    this.jobRepository.TryRemove(job);
                    return;
                }
            }

            try
            {
                await job.PublishAsync(titleManager);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.GetBaseException().Message, "JobManager");
                this.jobRepository.TryRemove(job);
                return;
            }

            this.jobRepository.TryRemove(job);
        }

        public JobManager(JobRepository jobRepository, TitleManager titleManager)
        {
            this.jobRepository = jobRepository;
            this.titleManager = titleManager;
        }
    }
}
