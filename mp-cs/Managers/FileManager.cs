﻿namespace mp_cs.Managers
{
    using System;
    using System.IO;
    using System.Linq;

    using mp_cs.Model.Titles;

    public class FileManager
    {
        public const string TemporaryDirectory = "tmp";

        public const string MediaDirectory = "media";

        private readonly string workDirectory;

        private readonly TitleManager titleManager;

        public FileManager(string workDirectory, TitleManager titleManager)
        {
            this.workDirectory = workDirectory;
            this.titleManager = titleManager;
        }

        public void EnsureDirectoryStateIsValid()
        {
            var presentDirectories = Directory.GetDirectories(this.workDirectory);

            // create necessary directory if they don't exist.
            if (!presentDirectories.Contains(TemporaryDirectory))
            {
                Directory.CreateDirectory(Path.Combine(this.workDirectory, TemporaryDirectory));
            }

            if (!presentDirectories.Contains(MediaDirectory))
            {
                Directory.CreateDirectory(Path.Combine(this.workDirectory, MediaDirectory));
            }

            var titles = this.titleManager.GetAllTitlesAsync().Result;

            foreach (var title in titles.OfType<LocalFileTitle>())
            {
                if (title.AudioFilePath != null)
                {
                    var audioPath = Path.Combine(this.workDirectory, title.AudioFilePath);
                    if (!File.Exists(audioPath))
                    {
                        title.HasAudio = false;
                        title.AudioFilePath = null;
                    }
                }

                if (title.VideoFilePath != null)
                {
                    var videoPath = Path.Combine(this.workDirectory, title.VideoFilePath);
                    if (!File.Exists(videoPath))
                    {
                        title.HasVideo = false;
                        title.VideoFilePath = null;
                    }
                }

                if (!title.HasAudio && !title.HasVideo)
                {
                    this.titleManager.RemoveTitleAsync(title.Id).Wait();
                }
            }
        }

        public static string CreateRandomSubDirectory(string parentDirectory)
        {
            var subDirectory = Path.Combine(parentDirectory, Path.GetRandomFileName());

            if (Directory.Exists(subDirectory))
            {
                throw new InvalidOperationException(String.Format("Directory \"{0}\" already exists, not starting job.", subDirectory));
            }

            Directory.CreateDirectory(subDirectory);
            return subDirectory;
        }
    }
}
