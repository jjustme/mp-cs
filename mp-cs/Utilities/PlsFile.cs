﻿namespace mp_cs.Utilities
{
    using System;
    using System.Collections.Generic;

    internal struct PlsFile
    {
        public IList<Uri> Files { get; set; }

        public IList<string> Titles { get; set; }

        public IList<long> Lengths { get; set; }
    }
}