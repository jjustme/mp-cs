namespace mp_cs.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    internal class PlsResponseFormatter : MediaTypeFormatter
    {
        private const string MediaTypeHeader = "audio/x-scpls";

        public override bool CanReadType(Type type)
        {
            return type == typeof(PlsFile);
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var fileLines = (await content.ReadAsStringAsync()).Split(new []{Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
            var files = new List<Uri>();

            foreach (var line in fileLines)
            {
                var match = Regex.Match(line, @"File\d+=(.*)$");
                if (match.Success)
                {
                    files.Add(new Uri(match.Groups[1].Value));
                }
            }

            return new PlsFile{ Files = files };
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public PlsResponseFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue(MediaTypeHeader));
        }
    }
}