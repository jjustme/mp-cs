﻿namespace mp_cs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;
    using Microsoft.Owin;
    using Microsoft.Owin.FileSystems;
    using Microsoft.Owin.StaticFiles;
    using Microsoft.Practices.ObjectBuilder2;
    using Microsoft.Practices.Unity;

    using mp_cs.Hub;
    using mp_cs.Managers;
    using mp_cs.Repositories;
    using mp_cs.Workers;

    using Owin;

    using Unity.WebApi;

    using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.UseFileServer(new FileServerOptions
            {
                RequestPath = PathString.Empty,
                FileSystem = new PhysicalFileSystem(@".\public"),
            });

            var container = new UnityContainer();

            appBuilder.MapSignalR(new HubConfiguration
            {
                Resolver = SetupSignalRDependencies(container)
            });

            // Configure Web API for self-host. 
            var config = new HttpConfiguration
            {
                DependencyResolver = SetupWebApiDependencies(container)
            };

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);

            PerformStartupChecks(container);
            Initialise(container);
        }

        private static void Initialise(IUnityContainer container)
        {
            var titleManager = container.Resolve<TitleManager>();
            titleManager.GetAllTitlesAsync().Result.ForEach(titleManager.ProcessTitle);
        }

        private static void PerformStartupChecks(IUnityContainer container)
        {
            var fileManager = container.Resolve<FileManager>();
            fileManager.EnsureDirectoryStateIsValid();
        }

        private static Microsoft.AspNet.SignalR.IDependencyResolver SetupSignalRDependencies(UnityContainer container)
        {
            var resolver = new UnitySignalRDependencyResolver(container);
            container.RegisterType<NotificationsHub>(new PerResolveLifetimeManager());
            container.RegisterType<IHubContext>(new InjectionFactory(_ => resolver.Resolve<IConnectionManager>().GetHubContext<NotificationsHub>()));
            return resolver;
        }

        private static IDependencyResolver SetupWebApiDependencies(IUnityContainer container)
        {
            var resolver = new UnityDependencyResolver(container);
            container.RegisterType<JobRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<JobManager>(new PerResolveLifetimeManager());
            container.RegisterType<TitlesContext>(new PerResolveLifetimeManager());
            container.RegisterType<TitleManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<TitlesRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<MetadataFetcher>(new ContainerControlledLifetimeManager());
            container.RegisterType<ClientsRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<FileManager>(new TransientLifetimeManager(), new InjectionConstructor(Program.WorkDirectory, resolver.GetService(typeof(TitleManager))));
            return resolver;
        }
    }

    public class UnitySignalRDependencyResolver : DefaultDependencyResolver
    {
        private readonly UnityContainer container;

        public override object GetService(Type serviceType)
        {
            return this.container.IsRegistered(serviceType) ? this.container.Resolve(serviceType) : base.GetService(serviceType);
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            return this.container.IsRegistered(serviceType) ? this.container.ResolveAll(serviceType).Concat(base.GetServices(serviceType)) : base.GetServices(serviceType);
        }

        public UnitySignalRDependencyResolver(UnityContainer container)
        {
            this.container = container;
        }
    }
}